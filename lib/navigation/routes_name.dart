class RouteNames {
  static const String welcomePage = '/';
  static const String homePage = '/home';
  static const String settlements = '/establecimientos';
  static const String settlementDetail = '/detalle_establecimiento';
}
