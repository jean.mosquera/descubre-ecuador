import 'package:descubreecuador/src/splashscreen/splash_page.dart';
import 'package:flutter/material.dart';
import 'package:descubreecuador/models/settlement.dart';
import 'package:descubreecuador/navigation/routes_name.dart';
import 'package:descubreecuador/src/home/home_page.dart';
import 'package:descubreecuador/src/settlement/settlement_page.dart';
import 'package:descubreecuador/src/settlement_list/settlement_list_pager.dart';

class AppRoutes {
  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case RouteNames.welcomePage:
        return MaterialPageRoute<dynamic>(builder: (_) => const SplashPage());
      case RouteNames.homePage:
        return MaterialPageRoute<dynamic>(builder: (_) => const HomePage());
      case RouteNames.settlements:
        return MaterialPageRoute<dynamic>(
            builder: (_) => const SettlementListPage());
      case RouteNames.settlementDetail:
        //obj is a Settlement object
        var obj = settings.arguments as Settlement;
        return MaterialPageRoute<dynamic>(
            builder: (_) => SettlementDetailPage(obj));

      default:
        return MaterialPageRoute<dynamic>(builder: (_) => const HomePage());
    }
  }
}
