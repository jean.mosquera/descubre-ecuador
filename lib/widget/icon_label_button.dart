import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class IconLabelButton extends StatelessWidget {
  final Function() onTap;
  final String label;
  final IconData icon;
  final Color color;
  final bool isAvailable;
  const IconLabelButton({
    Key? key,
    required this.onTap,
    required this.label,
    required this.icon,
    required this.color,
    required this.isAvailable,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: !isAvailable ? () {} : onTap,
      child: Container(
        height: 50,
        width: MediaQuery.of(context).size.width * 0.193,
        padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
        decoration: BoxDecoration(
            color: !isAvailable ? Colors.grey : color,
            borderRadius: const BorderRadius.all(Radius.circular(10))),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Icon(
                icon,
                color: Colors.white,
                size: MediaQuery.of(context).textScaleFactor * 20,
              ),
              Text(
                label,
                style: GoogleFonts.roboto(
                  fontSize: MediaQuery.of(context).textScaleFactor * 12,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
