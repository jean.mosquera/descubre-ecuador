import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ItemCategory extends StatelessWidget {
  final Color color;
  final IconData icon;
  final String category;
  const ItemCategory({
    Key? key,
    required this.color,
    required this.icon,
    required this.category,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: MediaQuery.of(context).size.height / 8,
          width: MediaQuery.of(context).size.width / 8,
          decoration: BoxDecoration(
              color: color,
              shape: BoxShape.circle,
              boxShadow: const [
                BoxShadow(
                    color: Colors.black12, blurRadius: 10, offset: Offset(0, 5))
              ]),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                icon,
                color: Colors.white,
                size: MediaQuery.of(context).size.height / 20,
              ),
            ],
          ),
        ),
        Text(
          category,
          style: GoogleFonts.lato(
              fontSize: MediaQuery.of(context).textScaleFactor * 15,
              color: Colors.black),
        )
      ],
    );
  }
}
