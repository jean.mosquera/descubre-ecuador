import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:descubreecuador/tools/colors.dart';

class ItemProvince extends StatelessWidget {
  final String image;
  final String name;
  final bool? isSelect;
  const ItemProvince({
    Key? key,
    required this.image,
    required this.name,
    this.isSelect = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          height: MediaQuery.of(context).size.height / 8,
          width: MediaQuery.of(context).size.width / 8,
          decoration: BoxDecoration(
              image: image != ""
                  ? DecorationImage(
                      //Image from internet
                      image: NetworkImage(image),
                      fit: BoxFit.contain,
                      colorFilter: isSelect!
                          ? ColorFilter.mode(
                              Colors.black.withOpacity(1), BlendMode.dstATop)
                          : ColorFilter.mode(
                              Colors.transparent.withOpacity(0.5),
                              BlendMode.dstATop))
                  : null,
              shape: BoxShape.circle,
              border: Border.all(
                color: isSelect! ? colors.green : Colors.transparent,
                width: 2,
              ),
              boxShadow: const [
                BoxShadow(
                    color: Colors.black12, blurRadius: 10, offset: Offset(0, 5))
              ]),
        ),
        Container(
          width: MediaQuery.of(context).size.width / 4,
          alignment: Alignment.center,
          child: Text(name,
              style: GoogleFonts.lato(
                  fontSize: MediaQuery.of(context).textScaleFactor * 15,
                  color: Colors.black),
              textAlign: TextAlign.center),
        )
      ],
    );
  }
}
