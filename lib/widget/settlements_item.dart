import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:descubreecuador/models/settlement.dart';
import 'package:descubreecuador/src/settlement/settlement_page.dart';

class SettlementItem extends StatelessWidget {
  final Settlement settlement;

  const SettlementItem({Key? key, required this.settlement}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //Card with the Settlement information
    return InkWell(
      onTap: (() {
        ///Navigate toSettlementDetailPage(settlement)
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => SettlementDetailPage(settlement)));
      }),
      child: Card(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20),
                bottomRight: Radius.circular(20))),
        elevation: 5,
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: MediaQuery.of(context).size.width * .9,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SizedBox(
                            width: MediaQuery.of(context).size.width * .6,
                            child: Text(
                              settlement.nombreComercial ?? "",
                              style: GoogleFonts.roboto(
                                fontSize:
                                    MediaQuery.of(context).textScaleFactor * 18,
                                fontWeight: FontWeight.w800,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width * .2,
                            child: Text(
                              settlement.categoria ?? "",
                              style: GoogleFonts.roboto(
                                fontSize:
                                    MediaQuery.of(context).textScaleFactor * 12,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Text(
                      settlement.clasificacion ?? "",
                      //)
                      style: GoogleFonts.roboto(
                          fontSize: MediaQuery.of(context).textScaleFactor * 12,
                          color: Colors.grey),
                    ),
                  ],
                ),
              ]),

              const SizedBox(
                height: 5,
              ),
              Row(children: [
                SizedBox(
                    width: MediaQuery.of(context).size.width * .6,
                    child: Text(
                      settlement.direccion ?? "",
                      style: GoogleFonts.roboto(
                        fontSize: MediaQuery.of(context).textScaleFactor * 12,
                      ),
                    )),
              ]),
              //ubicación canton provincia
              Row(children: [
                Text(
                  "${settlement.canton ?? ""} - ${settlement.provincia ?? ""}",
                  style: GoogleFonts.roboto(
                    fontSize: MediaQuery.of(context).textScaleFactor * 12,
                  ),
                ),
              ]),
            ],
          ),
        ),
      ),
    );
  }
}
