import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

/// Imports [libraries]
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:descubreecuador/models/city.dart';
import 'package:descubreecuador/models/settlement.dart';
import 'package:descubreecuador/models/province.dart';
import 'package:descubreecuador/tools/paths.dart';

// Definicion de provider para uso de la vista
final settlementsProvider = ChangeNotifierProvider<SettlementController>(
    (ref) => SettlementController());

class SettlementController extends ChangeNotifier {
//AppoimentList
  final List<Settlement> settlements = [];
  final List<Settlement> auxSettlements = [];
  bool loading = true;
  late String category = "";
  late String classification = "";
  late String province = "";
  late String canton = "";
  final bool isFiltering = false;
  final List<String> categories = [];
  final List<String> classifications = [];
  final List<String> provinces = [];
  final List<City> cantonsWithProvince = [];
  final List<String> cantons = [];
  bool showProvinces = true;
  late bool statusAppoinment;
  bool isFirstTime = true;

  getSettlementList() async {
    if (isFirstTime) {
      //open json file and read and parse to list _settlements
      var json = await rootBundle.loadString(paths.data);
      var list = jsonDecode(json);
      //jsonmap to list dynamic
      List<dynamic> listSettlements = list['establecimientos'];

      for (var element in listSettlements) {
        settlements.add(Settlement.fromJson(element));
      }
      auxSettlements.addAll(settlements);
      var categoriesAux = settlements.map((e) => e.categoria!).toSet().toList();
      categoriesAux.add("");
      categoriesAux.sort();
      categories.addAll(categoriesAux);
      var classificationsAux =
          settlements.map((e) => e.clasificacion!).toSet().toList();
      classificationsAux.add("");
      classificationsAux.sort();
      classifications.addAll(classificationsAux);

      var provincesAux = settlements.map((e) => e.provincia!).toSet().toList();
      provincesAux.add("");
      provincesAux.sort();
      provinces.addAll(provincesAux);
      List<City> cantonsAux = settlements
          .map((e) => City(name: e.canton, province: e.provincia))
          .toSet()
          .toList();
      cantonsAux.add(City(name: "", province: ""));
      cantonsAux.sort((a, b) => a.name!.compareTo(b.name!));
      cantonsWithProvince.addAll(cantonsAux);

      loading = false;
      isFirstTime = false;

      notifyListeners();
    }
  }

  showProvincesList(value) {
    showProvinces = value;
    notifyListeners();
  }

  List<Provinces> provincesListView = [
    Provinces(
        isSelect: true,
        name: "Todas",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Flag_of_Ecuador.svg/1280px-Flag_of_Ecuador.svg.png",
        shortName: "Todas"),
    Provinces(
        isSelect: false,
        name: "Azuay",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Bandera_Provincia_Azuay.svg/1024px-Bandera_Provincia_Azuay.svg.png",
        shortName: "Azuay"),
    Provinces(
        isSelect: false,
        name: "Bolívar",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/Bandera_Provincia_Bol%C3%ADvar.svg/750px-Bandera_Provincia_Bol%C3%ADvar.svg.png",
        shortName: "Bolívar"),
    Provinces(
        isSelect: false,
        name: "Cañar",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/8/85/Bandera_Provincia_Ca%C3%B1ar.svg/1280px-Bandera_Provincia_Ca%C3%B1ar.svg.png",
        shortName: "Cañar"),
    Provinces(
        isSelect: false,
        name: "Carchi",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/9/97/Bandera_Provincia_Carchi.svg/1125px-Bandera_Provincia_Carchi.svg.png",
        shortName: "Carchi"),
    Provinces(
        isSelect: false,
        name: "Chimborazo",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Bandera_Provincia_Chimborazo.svg/1280px-Bandera_Provincia_Chimborazo.svg.png",
        shortName: "Chimborazo"),
    Provinces(
        isSelect: false,
        name: "Cotopaxi",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Bandera_Provincia_Cotopaxi.svg/1280px-Bandera_Provincia_Cotopaxi.svg.png",
        shortName: "Cotopaxi"),
    Provinces(
        isSelect: false,
        name: "El Oro",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Bandera_Provincia_El_Oro.svg/1280px-Bandera_Provincia_El_Oro.svg.png",
        shortName: "El Oro"),
    Provinces(
        isSelect: false,
        name: "Esmeraldas",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Bandera_Provincia_Esmeraldas.svg/1280px-Bandera_Provincia_Esmeraldas.svg.png",
        shortName: "Esmeraldas"),
    Provinces(
        isSelect: false,
        name: "Galápagos",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Bandera_Provincia_Gal%C3%A1pagos.svg/1280px-Bandera_Provincia_Gal%C3%A1pagos.svg.png",
        shortName: "Galapagos"),
    Provinces(
        isSelect: false,
        name: "Guayas",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/Bandera_de_Guayaquil.svg/1280px-Bandera_de_Guayaquil.svg.png",
        shortName: "Guayas"),
    Provinces(
        isSelect: false,
        name: "Imbabura",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/Bandera_Provincia_Imbabura.svg/1280px-Bandera_Provincia_Imbabura.svg.png",
        shortName: "Imbabura"),
    Provinces(
        isSelect: false,
        name: "Loja",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/9/97/Bandera_Provincia_Loja.svg/1280px-Bandera_Provincia_Loja.svg.png",
        shortName: "Loja"),
    Provinces(
        isSelect: false,
        name: "Los Ríos",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Bandera_de_Los_R%C3%ADos.svg/1280px-Bandera_de_Los_R%C3%ADos.svg.png",
        shortName: "Los Rios"),
    Provinces(
        isSelect: false,
        name: "Manabí",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/6/68/Bandera_Provincia_Manab%C3%AD.svg/1280px-Bandera_Provincia_Manab%C3%AD.svg.png",
        shortName: "Manabí"),
    Provinces(
        isSelect: false,
        name: "Morona Santiago",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a5/Bandera_Provincia_Morona_Santiago.svg/1280px-Bandera_Provincia_Morona_Santiago.svg.png",
        shortName: "Morona Santiago"),
    Provinces(
        isSelect: false,
        name: "Napo",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6a/Bandera_Provincia_Napo.svg/1280px-Bandera_Provincia_Napo.svg.png",
        shortName: "Napo"),
    Provinces(
        isSelect: false,
        name: "Orellana",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0f/Bandera_Provincia_Orellana.svg/1280px-Bandera_Provincia_Orellana.svg.png",
        shortName: "Orellana"),
    Provinces(
        isSelect: false,
        name: "Pastaza",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Bandera_Provincia_Pastaza.svg/1280px-Bandera_Provincia_Pastaza.svg.png",
        shortName: "Pastaza"),
    Provinces(
        isSelect: false,
        name: "Pichincha",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Bandera_Provincia_Pichincha.svg/1280px-Bandera_Provincia_Pichincha.svg.png",
        shortName: "Pichincha"),
    Provinces(
        isSelect: false,
        name: "Santa Elena",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Bandera_Provincia_Santa_Elena.svg/1280px-Bandera_Provincia_Santa_Elena.svg.png",
        shortName: "Sta Elena"),
    Provinces(
        isSelect: false,
        name: "Santo Domingo de los Tsáchilas",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Bandera_Provincia_Santo_Domingo_de_los_Ts%C3%A1chilas.svg/1280px-Bandera_Provincia_Santo_Domingo_de_los_Ts%C3%A1chilas.svg.png",
        shortName: "Sto Domingo de los Tsáchilas"),
    Provinces(
        isSelect: false,
        name: "Sucumbíos",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c0/Bandera_Provincia_Sucumb%C3%ADos.svg/1280px-Bandera_Provincia_Sucumb%C3%ADos.svg.png",
        shortName: "Sucumbios"),
    Provinces(
        isSelect: false,
        name: "Tungurahua",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/Bandera_Provincia_Tungurahua.svg/1280px-Bandera_Provincia_Tungurahua.svg.png",
        shortName: "Tungurahua"),
    Provinces(
        isSelect: false,
        name: "Zamora Chinchipe",
        url:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/5/58/Bandera_Provincia_Zamora_Chinchipe.svg/1280px-Bandera_Provincia_Zamora_Chinchipe.svg.png",
        shortName: "Zamora Chinchipe"),
  ];

  filterByInput(String input) {
    loading = true;
    //get the value of provincesListView to filter the settlements when isSelect is true
    String selectProvince = provincesListView
        .where((element) => element.isSelect == true)
        .map((e) => e.name)
        .toString();
    var cleanSelectProvince = selectProvince.replaceAll('(', '');
    cleanSelectProvince = cleanSelectProvince.replaceAll(')', '');
    Iterable<Settlement> auxSettlementsRaw = auxSettlements;
    if (cleanSelectProvince == "Todas") {
      auxSettlementsRaw = auxSettlements;
    } else {
      auxSettlementsRaw = auxSettlements.where((element) =>
          element.provincia!.toLowerCase() ==
          cleanSelectProvince.toLowerCase());
    }

    notifyListeners();
    if (input.isNotEmpty) {
      settlements.clear();

      settlements.addAll(auxSettlementsRaw.where((element) => element
          .nombreComercial!
          .toLowerCase()
          .contains(input.toLowerCase())));

      loading = false;
      notifyListeners();
    } else {
      settlements.clear();
      settlements.addAll(auxSettlementsRaw);
      loading = false;
      notifyListeners();
    }
  }

  setLoading(value) {
    loading = value;
    notifyListeners();
  }

  filterByProvince(String province) {
    cantons.clear();
    cantons.addAll(cantonsWithProvince
        .where((element) =>
            element.province!.toLowerCase() == province.toLowerCase())
        .map((e) => e.name!)
        .toSet()
        .toList());
    cantons.add("");

    cantons.sort();

    notifyListeners();
  }

  filterDataByProvinces(String provinceV) {
    //Change isSelect to true where the name is equal to the province, other to false
    for (var element in provincesListView) {
      if (element.name == provinceV) {
        element.isSelect = true;
      } else {
        element.isSelect = false;
      }
    }

    settlements.clear();

    if (provinceV.contains("Todas")) {
      settlements.addAll(auxSettlements);
    } else {
      setProvince(provinceV);
      filterByProvince(provinceV);
      settlements.addAll(auxSettlements.where((element) =>
          element.provincia!.toLowerCase() == provinceV.toLowerCase() ||
          provinceV.isEmpty));
    }

    notifyListeners();
  }

  setIsFirstTime(value) {
    isFirstTime = value;
    notifyListeners();
  }

  setCategory(value) {
    category = value;
    notifyListeners();
  }

  setClassification(value) {
    classification = value;
    notifyListeners();
  }

  setProvince(value) {
    province = value;
    notifyListeners();
  }

  setCanton(value) {
    canton = value;
    notifyListeners();
  }

  filterData() {
    settlements.clear();
    settlements.addAll(auxSettlements.where((element) =>
        element.categoria!.toLowerCase() == category.toLowerCase() ||
        category.isEmpty));
    settlements.retainWhere((element) =>
        element.clasificacion!.toLowerCase() == classification.toLowerCase() ||
        classification.isEmpty);
    settlements.retainWhere((element) =>
        element.provincia!.toLowerCase() == province.toLowerCase() ||
        province.isEmpty);
    settlements.retainWhere((element) =>
        element.canton!.toLowerCase() == canton.toLowerCase() ||
        canton.isEmpty);
    notifyListeners();
  }

  clearFilters() {
    settlements.clear();
    settlements.addAll(auxSettlements);
    category = "";
    classification = "";
    province = "";
    canton = "";
    notifyListeners();
  }
}
