import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:descubreecuador/src/settlement_list/settlement_list_controller.dart';
import 'package:descubreecuador/tools/colors.dart';
import 'package:descubreecuador/widget/item_province.dart';
import 'package:descubreecuador/widget/settlements_item.dart';

class SettlementListPage extends ConsumerWidget {
  const SettlementListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var scrollControllerHorizontal = ScrollController();
    var provider = ref.watch(settlementsProvider);
    FirebaseAnalytics analytics = FirebaseAnalytics.instance;
    analytics.logEvent(name: 'settlement_list_page');
    provider.getSettlementList();
    return Scaffold(
        body: Stack(
      children: [
        Container(
          color: Colors.grey[200],
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            //Clip container bottom border rounded
            ClipRRect(
              borderRadius: const BorderRadius.only(
                bottomRight: Radius.circular(50),
                bottomLeft: Radius.circular(50),
              ),
              child: Container(
                  decoration: BoxDecoration(
                      color: colors.blue,
                      borderRadius: const BorderRadius.only()),
                  height: MediaQuery.of(context).size.height / 4.2,
                  width: double.infinity,
                  padding: const EdgeInsets.all(25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Bienvenido",
                        style: GoogleFonts.lato(
                            fontSize:
                                MediaQuery.of(context).textScaleFactor * 40,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        "La información proviene del Catastro Turistico del Ministerio de Turismo",
                        style: GoogleFonts.lato(
                            fontSize:
                                MediaQuery.of(context).textScaleFactor * 20,
                            color: Colors.white),
                      ),
                    ],
                  )),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height / 10,
            ),
            //Listview from categories with item_category widget
            Container(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Provincias",
                        style: GoogleFonts.lato(
                            fontSize:
                                MediaQuery.of(context).textScaleFactor * 20,
                            fontWeight: FontWeight.bold),
                      ),
                      InkWell(
                        onTap: () {
                          provider.showProvincesList(!provider.showProvinces);
                        },
                        child: Text(
                          provider.showProvinces ? "Ocultar" : "Mostrar",
                          style: GoogleFonts.lato(
                              fontSize:
                                  MediaQuery.of(context).textScaleFactor * 15,
                              fontWeight: FontWeight.bold,
                              color: colors.blue),
                        ),
                      ),
                    ],
                  ),
                  provider.showProvinces
                      ? SizedBox(
                          height: MediaQuery.of(context).size.height / 5,
                          child: Scrollbar(
                            controller: scrollControllerHorizontal,
                            child: ListView.builder(
                                controller: scrollControllerHorizontal,
                                scrollDirection: Axis.horizontal,
                                physics: const BouncingScrollPhysics(),
                                itemCount: provider.provincesListView.length,
                                itemBuilder: (context, index) {
                                  return InkWell(
                                    onTap: () {
                                      analytics.logEvent(
                                          name: 'province_filter',
                                          parameters: <String, dynamic>{
                                            'province': provider
                                                .provincesListView[index].name
                                          });
                                      provider.filterDataByProvinces(provider
                                          .provincesListView[index].name!);
                                    },
                                    child: ItemProvince(
                                        isSelect: provider
                                            .provincesListView[index].isSelect!,
                                        image: provider
                                            .provincesListView[index].url!,
                                        name: provider
                                            .provincesListView[index].name!),
                                  );
                                }),
                          ),
                        )
                      : Container(),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Establecimientos",
                    style: GoogleFonts.lato(
                        fontSize: MediaQuery.of(context).textScaleFactor * 20,
                        fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
            provider.loading
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : Expanded(
                    child: ListView.builder(
                        itemCount: provider.settlements.length,
                        itemBuilder: (context, index) {
                          return SettlementItem(
                              settlement: provider.settlements[index]);
                        }),
                  ),
          ]),
        ), //Search bar and filter
        Positioned(
          top: MediaQuery.of(context).size.height * .2,
          left: MediaQuery.of(context).size.width * .05,
          child: Container(
              height: MediaQuery.of(context).size.height * 0.1,
              width: MediaQuery.of(context).size.width * .9,
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(10)),
              child: Row(children: [
                //Container for search bar textform
                Expanded(
                  child: TextField(
                      decoration: const InputDecoration(
                        border: InputBorder.none,
                        hintText: "Buscar Establecimiento",
                      ),
                      onChanged: (value) {
                        provider.filterByInput(value);
                      }),
                ),

                const SizedBox(
                  width: 10,
                ),
                //Container for filter icon with overlay
                InkWell(
                    child: Container(
                      height: 40,
                      width: 40,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10)),
                      child: const Icon(Icons.filter_list),
                    ),
                    onTap: () {
                      showModalBottomSheet(
                          backgroundColor: Colors.transparent,
                          context: context,
                          builder: (context) {
                            return Consumer(builder: (context, ref, child) {
                              var provider = ref.watch(settlementsProvider);
                              return ClipRRect(
                                borderRadius: const BorderRadius.only(
                                  topLeft: Radius.circular(20),
                                  topRight: Radius.circular(20),
                                ),
                                child: Container(
                                    height: MediaQuery.of(context).size.height *
                                        .45,
                                    width: MediaQuery.of(context).size.width,
                                    color: colors.white,
                                    padding: const EdgeInsets.only(
                                        top: 20,
                                        left: 20,
                                        right: 20,
                                        bottom: 20),
                                    child: SingleChildScrollView(
                                      child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            //Filtros por clasificación provincia y canton
                                            Text(
                                              "Filtros",
                                              style: GoogleFonts.lato(
                                                  fontSize:
                                                      MediaQuery.of(context)
                                                              .textScaleFactor *
                                                          20,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              "Clasificación",
                                              style: GoogleFonts.lato(
                                                  fontSize:
                                                      MediaQuery.of(context)
                                                              .textScaleFactor *
                                                          15,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            //drOPDOWN
                                            SizedBox(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  .9,
                                              child: DropdownButton(
                                                value: provider.classification,
                                                items: provider.classifications
                                                    .map((e) => DropdownMenuItem(
                                                        value: e,
                                                        child: SizedBox(
                                                            width: MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width *
                                                                .8,
                                                            child: Text(e))))
                                                    .toList(),
                                                onChanged: (value) {
                                                  provider
                                                      .setClassification(value);
                                                },
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              "Provincia",
                                              style: GoogleFonts.lato(
                                                  fontSize:
                                                      MediaQuery.of(context)
                                                              .textScaleFactor *
                                                          15,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            //drOPDOWN
                                            SizedBox(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  .9,
                                              child: DropdownButton(
                                                value: provider.province
                                                    .toLowerCase(),
                                                items: provider.provinces
                                                    .map((e) => DropdownMenuItem(
                                                        value: e.toLowerCase(),
                                                        child: SizedBox(
                                                            width: MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width *
                                                                .8,
                                                            child: Text(e))))
                                                    .toList(),
                                                onChanged: (value) {
                                                  provider.setProvince(value);
                                                  provider
                                                      .filterDataByProvinces(
                                                          value!);
                                                },
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              "Cantón",
                                              style: GoogleFonts.lato(
                                                  fontSize:
                                                      MediaQuery.of(context)
                                                              .textScaleFactor *
                                                          15,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            //drOPDOWN
                                            SizedBox(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  .9,
                                              child: DropdownButton(
                                                value: provider.canton,
                                                items: provider.cantons
                                                    .map((e) => DropdownMenuItem(
                                                        value: e,
                                                        child: SizedBox(
                                                            width: MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width *
                                                                .8,
                                                            child: Text(e))))
                                                    .toList(),
                                                onChanged: (value) {
                                                  provider.setCanton(value);
                                                },
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            //bUTTONS TO APPLY FILTERS
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                ElevatedButton(
                                                    onPressed: () {
                                                      provider.clearFilters();
                                                      Navigator.pop(context);
                                                    },
                                                    child:
                                                        const Text("Limpiar")),
                                                ElevatedButton(
                                                    onPressed: () {
                                                      provider.filterData();
                                                      Navigator.pop(context);
                                                    },
                                                    child:
                                                        const Text("Aplicar"))
                                              ],
                                            )
                                          ]),
                                    )),
                              );
                            });
                          });
                    }),
              ])),
        ),
      ],
    ));
  }
}
