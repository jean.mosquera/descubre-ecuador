import 'package:descubreecuador/tools/colors.dart';
import 'package:descubreecuador/tools/paths.dart';
import 'package:flutter/material.dart';
import 'package:rive/rive.dart';

//Splash screen rive with timer 3 seconds
class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 5), () {
      Navigator.pushReplacementNamed(context, '/home');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: colors.blueDark,
        child: Padding(
          padding: const EdgeInsets.all(30.0),
          child: Center(
            child: RiveAnimation.asset(visualResources.loaderRiv),
          ),
        ),
      ),
    );
  }
}
