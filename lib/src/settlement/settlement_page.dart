import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:share_plus/share_plus.dart';
import 'package:descubreecuador/models/settlement.dart';
import 'package:descubreecuador/tools/colors.dart';
import 'package:descubreecuador/widget/icon_label_button.dart';
import 'package:url_launcher/url_launcher.dart';

class SettlementDetailPage extends ConsumerWidget {
  const SettlementDetailPage(this.settlement, {Key? key}) : super(key: key);

  final Settlement settlement;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    FirebaseAnalytics analytics = FirebaseAnalytics.instance;
    analytics.logEvent(
      name: 'settlement_detail',
      parameters: <String, dynamic>{
        'settlement_name': settlement.nombreComercial,
      },
    );
    return Scaffold(
        body: Stack(
      children: [
        //background bottom text with opacity with the name of settlement
        Positioned(
          bottom: 0,
          right: 0,
          child: Icon(
            FontAwesomeIcons.store,
            color: colors.blue.withOpacity(.5),
            size: MediaQuery.of(context).size.height * .3,
          ),
        ),
        Container(
          color: Colors.white.withOpacity(.7),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //Clip container bottom border rounded
              ClipRRect(
                borderRadius: const BorderRadius.only(
                  bottomRight: Radius.circular(49),
                  bottomLeft: Radius.circular(49),
                ),
                child: Container(
                    decoration: BoxDecoration(
                        color: colors.blue,
                        borderRadius: const BorderRadius.only()),
                    height: MediaQuery.of(context).size.height / 6.5,
                    width: double.infinity,
                    padding: const EdgeInsets.all(25),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            children: [
                              IconButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  icon: const Icon(
                                    Icons.arrow_back,
                                    color: Colors.white,
                                  )),
                              SizedBox(
                                width: MediaQuery.of(context).size.width * 0.7,
                                height: MediaQuery.of(context).size.height / 20,
                                child: Text(
                                  settlement.nombreComercial ??
                                      "Establecimiento",
                                  style: GoogleFonts.lato(
                                      fontSize: MediaQuery.of(context)
                                              .textScaleFactor *
                                          25,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                        ],
                      ),
                    )),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height / 10,
              ),
              Container(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: const EdgeInsets.only(left: 20),
                        child: Text(
                          "Nombre Comercial",
                          style: GoogleFonts.lato(
                              fontSize:
                                  MediaQuery.of(context).textScaleFactor * 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 30),
                        child: Text(
                          settlement.nombreComercial == null ||
                                  settlement.nombreComercial == ""
                              ? "Sin nombre"
                              : settlement.nombreComercial!,
                          style: GoogleFonts.lato(
                              fontSize:
                                  MediaQuery.of(context).textScaleFactor * 15,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      //Clasificacion
                      Container(
                        padding: const EdgeInsets.only(left: 20),
                        child: Text(
                          "Clasificacion/Categoria",
                          style: GoogleFonts.lato(
                              fontSize:
                                  MediaQuery.of(context).textScaleFactor * 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 30),
                        child: Text(
                          "${settlement.clasificacion == null || settlement.clasificacion == "" ? "Sin clasificación" : settlement.clasificacion}- ${settlement.categoria == null || settlement.categoria == "" ? "Categoria no disponible " : settlement.categoria}",
                          style: GoogleFonts.lato(
                              fontSize:
                                  MediaQuery.of(context).textScaleFactor * 15,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      //numeroRegistro

                      Container(
                        padding: const EdgeInsets.only(left: 20),
                        child: Text(
                          "Numero de registro",
                          style: GoogleFonts.lato(
                              fontSize:
                                  MediaQuery.of(context).textScaleFactor * 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 30),
                        child: Text(
                          settlement.numeroRegistro == null ||
                                  settlement.numeroRegistro == ""
                              ? "Sin registro "
                              : settlement.numeroRegistro!,
                          style: GoogleFonts.lato(
                              fontSize:
                                  MediaQuery.of(context).textScaleFactor * 15,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      //Address
                      Container(
                        padding: const EdgeInsets.only(left: 20),
                        child: Text(
                          "Dirección",
                          style: GoogleFonts.lato(
                              fontSize:
                                  MediaQuery.of(context).textScaleFactor * 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 30),
                        child: Text(
                          settlement.direccion == null ||
                                  settlement.direccion == ""
                              ? "Direccion no disponible"
                              : settlement.direccion!,
                          style: GoogleFonts.lato(
                              fontSize:
                                  MediaQuery.of(context).textScaleFactor * 15,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),

                      ///parroquia/canton/provincia
                      Container(
                        padding: const EdgeInsets.only(left: 20),
                        child: Text(
                          "Parroquia/Pronvicia/Canton",
                          style: GoogleFonts.lato(
                              fontSize:
                                  MediaQuery.of(context).textScaleFactor * 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 30),
                        child: Text(
                          "${settlement.parroquia == null || settlement.parroquia == "" ? "No definido" : settlement.parroquia}/ ${settlement.canton == null || settlement.canton == "" ? "No definido" : settlement.canton}/ ${settlement.provincia == null || settlement.provincia == "" ? "No definido" : settlement.provincia}",
                          style: GoogleFonts.lato(
                              fontSize:
                                  MediaQuery.of(context).textScaleFactor * 15,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      //Phone
                      Container(
                        padding: const EdgeInsets.only(left: 20),
                        child: Text(
                          "Telefono principal",
                          style: GoogleFonts.lato(
                              fontSize:
                                  MediaQuery.of(context).textScaleFactor * 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 30),
                        child: Text(
                          settlement.telefonoPrincipal == null ||
                                  settlement.telefonoPrincipal == ""
                              ? "Telefono no disponible"
                              : settlement.telefonoPrincipal!,
                          style: GoogleFonts.lato(
                              fontSize:
                                  MediaQuery.of(context).textScaleFactor * 15,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      //Email
                      Container(
                        padding: const EdgeInsets.only(left: 20),
                        child: Text(
                          "Email",
                          style: GoogleFonts.lato(
                              fontSize:
                                  MediaQuery.of(context).textScaleFactor * 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 30),
                        child: Text(
                          settlement.correoElectronico == null ||
                                  settlement.correoElectronico == ""
                              ? "Correo electronico no disponible"
                              : settlement.correoElectronico!,
                          style: GoogleFonts.lato(
                              fontSize:
                                  MediaQuery.of(context).textScaleFactor * 15,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      //Web
                      Container(
                        padding: const EdgeInsets.only(left: 20),
                        child: Text(
                          "Direccion web",
                          style: GoogleFonts.lato(
                              fontSize:
                                  MediaQuery.of(context).textScaleFactor * 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 30),
                        child: Text(
                          settlement.direccionWeb == null ||
                                  settlement.direccionWeb == ""
                              ? "Web no disponible"
                              : settlement.direccionWeb!,
                          style: GoogleFonts.lato(
                              fontSize:
                                  MediaQuery.of(context).textScaleFactor * 15,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      //
                      Container(
                        padding: const EdgeInsets.only(left: 20),
                        child: Text(
                          "Mas opciones",
                          style: GoogleFonts.lato(
                              fontSize:
                                  MediaQuery.of(context).textScaleFactor * 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      //
                      Container(
                        padding: const EdgeInsets.only(left: 30),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            InkWell(
                              onTap: () {
                                analytics.logEvent(
                                    name: "llamar",
                                    parameters: <String, dynamic>{
                                      "nombre": settlement.nombreComercial,
                                      "canton": settlement.canton,
                                      "provincia": settlement.provincia,
                                    });
                                Share.share(
                                    "Hola, te comparto la informacion de ${settlement.nombreComercial} ubicado en ${settlement.direccion}/${settlement.canton}-${settlement.provincia}. Numero de contacto: ${settlement.telefonoPrincipal},google maps:  https://www.google.com/maps/search/?api=1&query=${"${settlement.nombreComercial} ${settlement.provincia} Ecuador"}");
                              },
                              child: Row(children: const [
                                Icon(FontAwesomeIcons.share),
                                SizedBox(
                                  width: 10,
                                ),
                                Text("Compartir")
                              ]),
                            ),
                            //Copiar
                            InkWell(
                              onTap: () {
                                analytics.logEvent(
                                    name: "copiar_info",
                                    parameters: <String, dynamic>{
                                      "nombre": settlement.nombreComercial,
                                      "canton": settlement.canton,
                                      "provincia": settlement.provincia,
                                    });
                                Clipboard.setData(ClipboardData(
                                    text:
                                        "${settlement.nombreComercial} ubicado en ${settlement.direccion}/${settlement.canton}-${settlement.provincia}. Numero de contacto: ${settlement.telefonoPrincipal},google maps:  https://www.google.com/maps/search/?api=1&query=${"${settlement.nombreComercial} ${settlement.provincia} Ecuador"}"));
                                ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                        content: Text(
                                            "Informacion copiada al portapapeles")));
                              },
                              child: Row(children: const [
                                Icon(FontAwesomeIcons.copy),
                                SizedBox(
                                  width: 10,
                                ),
                                Text("Copiar")
                              ]),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )),
            ],
          ),
        ),
        Positioned(
          top: MediaQuery.of(context).size.height * .1,
          left: MediaQuery.of(context).size.width * .05,
          child: Container(
              height: MediaQuery.of(context).size.height * 0.1,
              width: MediaQuery.of(context).size.width * .9,
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(10)),
              child:
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                //Geolocate, call, email, web options icon label buttons
                IconLabelButton(
                    icon: Icons.location_on,
                    label: "Ubicar",
                    isAvailable: settlement.direccion != null,
                    color: Colors.green,
                    onTap: () {
                      analytics
                          .logEvent(name: "settlement_geolocate", parameters: {
                        "settlement_name": settlement.nombreComercial,
                        "settlement_province": settlement.provincia,
                      });
                      var uri = Uri.parse(
                          "https://www.google.com/maps/search/?api=1&query=${"${settlement.nombreComercial} ${settlement.provincia} Ecuador"}");
                      launchUrl(uri);
                    }),
                const SizedBox(
                  width: 10,
                ),
                IconLabelButton(
                    icon: Icons.call,
                    label: "Llamar",
                    isAvailable: settlement.telefonoPrincipal != null ||
                        settlement.telefonoPrincipal != "",
                    color: Colors.blue,
                    onTap: () {
                      analytics.logEvent(name: "settlement_call", parameters: {
                        "settlement_name": settlement.nombreComercial,
                        "settlement_province": settlement.provincia,
                      });
                      var uri = Uri.parse(
                        "tel:${settlement.telefonoPrincipal}",
                      );
                      launchUrl(uri);
                    }),
                const SizedBox(
                  width: 10,
                ),
                IconLabelButton(
                    icon: Icons.email,
                    label: "Correo",
                    isAvailable: settlement.correoElectronico != null ||
                        settlement.correoElectronico != "",
                    color: Colors.blue,
                    onTap: () {
                      analytics.logEvent(name: "settlement_email", parameters: {
                        "settlement_name": settlement.nombreComercial,
                        "settlement_province": settlement.provincia,
                      });
                      var uri =
                          Uri.parse("mailto:${settlement.correoElectronico}");
                      launchUrl(uri);
                    }),
                const SizedBox(
                  width: 10,
                ),
                IconLabelButton(
                    //Icon www
                    icon: Icons.web,
                    label: "Web",
                    isAvailable: true,
                    color: Colors.blue,
                    onTap: () async {
                      analytics.logEvent(name: "settlement_web", parameters: {
                        "settlement_name": settlement.nombreComercial,
                        "settlement_province": settlement.provincia,
                      });
                      var url = "";

                      if (settlement.direccionWeb == null ||
                          settlement.direccionWeb == "") {
                        url =
                            "https://www.google.com/search?q=${"${settlement.nombreComercial} ${settlement.provincia} Ecuador"}";
                      } else {
                        url =
                            "https://www.google.com/search?q=${"${settlement.nombreComercial} ${settlement.provincia} Ecuador"}";
                      }

                      var uri = Uri.parse(url);
                      //has url true open in browser
                      launchUrl(uri);
                    }),
              ])),
        ),
      ],
    ));
  }
}
