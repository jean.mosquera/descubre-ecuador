import 'package:flutter/material.dart';

/// Imports [libraries]
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:descubreecuador/models/region.dart';

// Definicion de provider para uso de la vista
final homeProvider =
    ChangeNotifierProvider<HomeController>((ref) => HomeController());

class HomeController extends ChangeNotifier {
  String urlImage = ''
      "https://images.pexels.com/photos/6276301/pexels-photo-6276301.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2";
  var selectedRegion = 0;
  List<String> images = [
    "https://images.pexels.com/photos/6276301/pexels-photo-6276301.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
    "https://images.pexels.com/photos/5293883/pexels-photo-5293883.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
    "https://images.pexels.com/photos/4891078/pexels-photo-4891078.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
    "https://images.pexels.com/photos/13142252/pexels-photo-13142252.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
    "https://images.pexels.com/photos/10824356/pexels-photo-10824356.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"
  ];
  List<Region> regions = [
    Region(
        name: "Costa",
        url:
            "https://images.pexels.com/photos/13142252/pexels-photo-13142252.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"),
    Region(
        name: "Sierra",
        url:
            "https://images.pexels.com/photos/6276301/pexels-photo-6276301.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"),
    Region(
        name: "Oriente",
        url:
            "https://images.pexels.com/photos/6034712/pexels-photo-6034712.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"),
    Region(
        name: "Galapagos",
        url:
            "https://images.pexels.com/photos/1190690/pexels-photo-1190690.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"),
  ];

  List<Color> colors = [Colors.yellow, Colors.blue, Colors.red, Colors.white];

  late Color color = colors[0];

  void setUrlImage(String url) {
    urlImage = url;
    notifyListeners();
  }

  void setRegion(int index) {
    selectedRegion = index;
    notifyListeners();
  }

  //recursive method to change the image
  void changeImage() {
    Future.delayed(const Duration(seconds: 15), () {
      if (images.indexOf(urlImage) == images.length - 1) {
        setUrlImage(images[0]);
      } else {
        setUrlImage(images[images.indexOf(urlImage) + 1]);
      }
      changeImage();
    });
  }

  //method to change the color from the aray
  void changeColor() {
    Future.delayed(const Duration(seconds: 15), () {
      if (colors.indexOf(color) == colors.length - 1) {
        color = colors[0];
      } else {
        color = colors[colors.indexOf(color) + 1];
      }
      notifyListeners();
      changeColor();
    });
  }
}
