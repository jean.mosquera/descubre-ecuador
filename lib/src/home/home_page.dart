import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:descubreecuador/navigation/routes_name.dart';
import 'package:descubreecuador/src/home/home_controller.dart';
import 'package:descubreecuador/tools/colors.dart';
import 'package:descubreecuador/tools/size_config.dart';

class HomePage extends ConsumerWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    SizeConfig().init(context);
    var provider = ref.watch(homeProvider);
    FirebaseAnalytics analytics = FirebaseAnalytics.instance;
    analytics.logEvent(name: 'home_page');
    return Scaffold(
        //Personalized AppBar

        body: Scaffold(
            body: Stack(
      children: [
        //Image full screen
        Image(
          image: NetworkImage(
            provider.regions[provider.selectedRegion].url!,
          ),
          width: double.infinity,
          height: double.infinity,
          fit: BoxFit.cover,
          //with opacity
          color: colors.black.withOpacity(.4),
          colorBlendMode: BlendMode.darken,
        ),
        //Bottom container with text
        Container(
            padding: EdgeInsets.only(
              left: MediaQuery.of(context).size.width / 10,
              right: MediaQuery.of(context).size.width / 10,
            ),
            margin:
                EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Todos los establecimientos de Ecuador en un solo lugar",
                  style: GoogleFonts.roboto(
                      fontSize: MediaQuery.of(context).size.height / 25,
                      color: colors.white,
                      fontWeight: FontWeight.w600),
                ),
                //Region name
                Text(
                  provider.regions[provider.selectedRegion].name!,
                  style: GoogleFonts.roboto(
                      fontSize: MediaQuery.of(context).size.height / 40,
                      color: colors.white,
                      fontWeight: FontWeight.w200),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height / 20,
                ),
                //Change image
                Container(
                  decoration: BoxDecoration(
                      color: colors.black.withOpacity(.3),
                      borderRadius: BorderRadius.circular(10)),
                ),
                Container(
                  padding: const EdgeInsets.only(top: 10, bottom: 10),
                  decoration:
                      //black container with opacity
                      BoxDecoration(
                          color: colors.black.withOpacity(.3),
                          borderRadius: BorderRadius.circular(10)),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height / 10,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: provider.regions.length,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            provider.setRegion(index);
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width / 5,
                            height: MediaQuery.of(context).size.height / 10,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image:
                                    NetworkImage(provider.regions[index].url!),
                                fit: BoxFit.cover,
                              ),
                              shape: BoxShape.circle,
                            ),
                          ),
                        );
                      }),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height / 25,
                ),
                //Continuar
                InkWell(
                  onTap: () {
                    Navigator.pushNamed(context, RouteNames.settlements);
                  },
                  child: Container(
                    padding: const EdgeInsets.all(10),
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 15,
                    decoration: BoxDecoration(
                        color: colors.white.withOpacity(.1),
                        borderRadius: BorderRadius.circular(10)),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Continuar",
                            style: GoogleFonts.roboto(
                                fontSize:
                                    MediaQuery.of(context).size.height / 40,
                                color: colors.white),
                          ),
                          Icon(
                            Icons.arrow_forward,
                            color: colors.white,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            )),
      ],
    )));
  }
}
