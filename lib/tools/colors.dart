import 'package:flutter/material.dart';

class CustomColors {
  final Color blue = const Color(0xFF034EA2);
  final Color yellow = const Color(0xFFFFDD00);
  final Color red = const Color(0xFFED1C24);
  final Color green = const Color(0xFF086F35);
  final Color white = const Color(0xFFFFFFFF);
  final Color black = const Color(0xFF000000);
  final Color blueDark = const Color(0xFF000732);
}

CustomColors colors = CustomColors();
