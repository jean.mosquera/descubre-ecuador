class Paths {
  final String data = 'assets/data/catastro_mintur.json';
}

Paths paths = Paths();

class VisualResources {
  final String ecMap = 'assets/images/ec_map.svg';
  final String background = 'assets/images/background.svg';
  final String backgroundPng = 'assets/images/background.png';
  final String loaderRiv = 'assets/anim/loader.riv';
}

VisualResources visualResources = VisualResources();
