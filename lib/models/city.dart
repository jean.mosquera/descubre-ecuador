class City {
  String? name;
  String? province;

  City({this.name, this.province});

  factory City.fromJson(Map<String, dynamic> json) {
    return City(
      name: json['name'],
      province: json['province'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['province'] = province;
    return data;
  }

  @override
  String toString() {
    return 'City{name: $name, province: $province}';
  }
}
