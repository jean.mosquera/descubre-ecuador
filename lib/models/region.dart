class Region {
  String? url;
  String? name;

  Region({this.url, this.name});

  factory Region.fromJson(Map<String, dynamic> json) {
    return Region(
      url: json['url'],
      name: json['name'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['url'] = url;
    data['name'] = name;
    return data;
  }

  @override
  String toString() {
    return 'Region{url: $url, name: $name}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Region &&
          runtimeType == other.runtimeType &&
          url == other.url &&
          name == other.name;

  @override
  int get hashCode => url.hashCode ^ name.hashCode;
}
