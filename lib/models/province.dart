class Provinces {
  String? url;
  String? name;
  String? shortName;
  bool? isSelect;

  Provinces({
    this.isSelect,
    this.url,
    this.name,
    this.shortName,
  });

  Provinces.fromJson(Map<String, dynamic> json, this.isSelect)
      : url = json['url'],
        name = json['name'],
        shortName = json['shortName'];

  Map<String, dynamic> toJson() => {
        'url': url,
        'name': name,
        'shortName': shortName,
      };

  @override
  String toString() {
    return 'Provinces{url: $url, name: $name, shortName: $shortName}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Provinces &&
          runtimeType == other.runtimeType &&
          url == other.url &&
          name == other.name &&
          shortName == other.shortName &&
          isSelect == other.isSelect;

  @override
  int get hashCode =>
      url.hashCode ^ name.hashCode ^ shortName.hashCode ^ isSelect.hashCode;
}
