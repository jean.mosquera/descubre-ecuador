/// Nombre Comercial	Número de Registro	Clasificación	Categoría	Provincia	Cantón	Parroquia	Dirección	Teléfono Principal	Correo Electrónico	Dirección Web
class Settlement {
  String? nombreComercial;
  String? numeroRegistro;
  String? clasificacion;
  String? categoria;
  String? provincia;
  String? canton;
  String? parroquia;
  String? direccion;
  String? telefonoPrincipal;
  String? correoElectronico;
  String? direccionWeb;

  Settlement(
      {this.nombreComercial,
      this.numeroRegistro,
      this.clasificacion,
      this.categoria,
      this.provincia,
      this.canton,
      this.parroquia,
      this.direccion,
      this.telefonoPrincipal,
      this.correoElectronico,
      this.direccionWeb});

  Settlement.fromJson(Map<String, dynamic> json) {
    nombreComercial = json['Nombre Comercial'] ?? '';
    numeroRegistro = json['Número de Registro'] ?? '';
    clasificacion = json['Clasificación'] ?? '';
    categoria = json['Categoría'] ?? '';
    provincia = json['Provincia'] ?? '';
    canton = json['Cantón'];
    parroquia = json['Parroquia'] ?? '';
    direccion = json['Dirección'] ?? '';
    telefonoPrincipal = json['Teléfono Principal'] ?? '';
    correoElectronico = json['Correo Electrónico'] ?? '';
    direccionWeb = json['Dirección Web'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Nombre Comercial'] = nombreComercial;
    data['Número de Registro'] = numeroRegistro;
    data['Clasificación'] = clasificacion;
    data['Categoría'] = categoria;
    data['Provincia'] = provincia;
    data['Cantón'] = canton;
    data['Parroquia'] = parroquia;
    data['Dirección'] = direccion;
    data['Teléfono Principal'] = telefonoPrincipal;
    data['Correo Electrónico'] = correoElectronico;
    data['Dirección Web'] = direccionWeb;
    return data;
  }

  @override
  String toString() {
    return 'Settlement{nombreComercial: $nombreComercial, numeroRegistro: $numeroRegistro, clasificacion: $clasificacion, categoria: $categoria, provincia: $provincia, canton: $canton, parroquia: $parroquia, direccion: $direccion, telefonoPrincipal: $telefonoPrincipal, correoElectronico: $correoElectronico, direccionWeb: $direccionWeb}';
  }
}
