import 'package:descubreecuador/models/region.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  group("Test Region Model ", () {
    test("Test Constructor", () {
      final region = Region(url: "url", name: "name");
      expect(region.url, "url");
      expect(region.name, "name");
    });

    test("Test toJson", () {
      final region = Region(url: "url", name: "name");
      expect(region.toJson(), {"url": "url", "name": "name"});
    });

    test("Test fromJson", () {
      final region = Region.fromJson({"url": "url", "name": "name"});
      expect(region.url, "url");
      expect(region.name, "name");
    });

    test("Test toString", () {
      final region = Region(url: "url", name: "name");
      expect(region.toString(), "Region{url: url, name: name}");
    });

    test("Test operator ==", () {
      final region = Region(url: "url", name: "name");
      final region2 = Region(url: "url", name: "name");
      expect(region == region2, true);
    });

    test("Test hashCode", () {
      final region = Region(url: "url", name: "name");
      expect(region.hashCode, 451952792);
    });
  });
}
