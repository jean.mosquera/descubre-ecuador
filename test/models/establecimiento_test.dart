import 'package:descubreecuador/models/settlement.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  group("Test Slettment", () {
    //test constructor
    test("Test Constructor", () {
      final establecimiento = Settlement(
          nombreComercial: "nombreComercial",
          numeroRegistro: "numeroRegistro",
          clasificacion: "clasificacion",
          categoria: "categoria",
          provincia: "provincia",
          canton: "canton",
          parroquia: "parroquia",
          direccion: "direccion",
          telefonoPrincipal: "telefonoPrincipal",
          correoElectronico: "correoElectronico",
          direccionWeb: "direccionWeb");
      expect(establecimiento.nombreComercial, "nombreComercial");
      expect(establecimiento.numeroRegistro, "numeroRegistro");
      expect(establecimiento.clasificacion, "clasificacion");
      expect(establecimiento.categoria, "categoria");
      expect(establecimiento.provincia, "provincia");
      expect(establecimiento.canton, "canton");
      expect(establecimiento.parroquia, "parroquia");
      expect(establecimiento.direccion, "direccion");
      expect(establecimiento.telefonoPrincipal, "telefonoPrincipal");
      expect(establecimiento.correoElectronico, "correoElectronico");
      expect(establecimiento.direccionWeb, "direccionWeb");
    });

    test("Test fromJson", () {
      final establecimiento = Settlement.fromJson({
        "Nombre Comercial": "nombreComercial",
        "Número de Registro": "numeroRegistro",
        "Clasificación": "clasificacion",
        "Categoría": "categoria",
        "Provincia": "provincia",
        "Cantón": "canton",
        "Parroquia": "parroquia",
        "Dirección": "direccion",
        "Teléfono Principal": "telefonoPrincipal",
        "Correo Electrónico": "correoElectronico",
        "Dirección Web": "direccionWeb"
      });
      expect(establecimiento.nombreComercial, "nombreComercial");
      expect(establecimiento.numeroRegistro, "numeroRegistro");
      expect(establecimiento.clasificacion, "clasificacion");
      expect(establecimiento.categoria, "categoria");
      expect(establecimiento.provincia, "provincia");
      expect(establecimiento.canton, "canton");
      expect(establecimiento.parroquia, "parroquia");
      expect(establecimiento.direccion, "direccion");
      expect(establecimiento.telefonoPrincipal, "telefonoPrincipal");
      expect(establecimiento.correoElectronico, "correoElectronico");
      expect(establecimiento.direccionWeb, "direccionWeb");
    });

    test("Test toJson", () {
      final establecimiento = Settlement(
          nombreComercial: "nombreComercial",
          numeroRegistro: "numeroRegistro",
          clasificacion: "clasificacion",
          categoria: "categoria",
          provincia: "provincia",
          canton: "canton",
          parroquia: "parroquia",
          direccion: "direccion",
          telefonoPrincipal: "telefonoPrincipal",
          correoElectronico: "correoElectronico",
          direccionWeb: "direccionWeb");
      final json = establecimiento.toJson();
      expect(json["Nombre Comercial"], "nombreComercial");
      expect(json["Número de Registro"], "numeroRegistro");
      expect(json["Clasificación"], "clasificacion");
      expect(json["Categoría"], "categoria");
      expect(json["Provincia"], "provincia");
      expect(json["Cantón"], "canton");
      expect(json["Parroquia"], "parroquia");
      expect(json["Dirección"], "direccion");
      expect(json["Teléfono Principal"], "telefonoPrincipal");
      expect(json["Correo Electrónico"], "correoElectronico");
      expect(json["Dirección Web"], "direccionWeb");
    });

    test("Test toString", () {
      final establecimiento = Settlement(
          nombreComercial: "nombreComercial",
          numeroRegistro: "numeroRegistro",
          clasificacion: "clasificacion",
          categoria: "categoria",
          provincia: "provincia",
          canton: "canton",
          parroquia: "parroquia",
          direccion: "direccion",
          telefonoPrincipal: "telefonoPrincipal",
          correoElectronico: "correoElectronico",
          direccionWeb: "direccionWeb");
      expect(establecimiento.toString(),
          "Settlement{nombreComercial: nombreComercial, numeroRegistro: numeroRegistro, clasificacion: clasificacion, categoria: categoria, provincia: provincia, canton: canton, parroquia: parroquia, direccion: direccion, telefonoPrincipal: telefonoPrincipal, correoElectronico: correoElectronico, direccionWeb: direccionWeb}");
    });
  });
}
