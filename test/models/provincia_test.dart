import 'package:descubreecuador/models/province.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group("Test Provincia Model", () {
    //test constructor
    test("Test Constructor", () {
      final provincia = Provinces(
        isSelect: true,
        url: "url",
        name: "name",
        shortName: "shortName",
      );
      expect(provincia.isSelect, true);
      expect(provincia.url, "url");
      expect(provincia.name, "name");
      expect(provincia.shortName, "shortName");
    });
    //test toJson
    test("Test toJson", () {
      final provincia = Provinces(
        isSelect: true,
        url: "url",
        name: "name",
        shortName: "shortName",
      );
      expect(provincia.toJson(), {
        'url': "url",
        'name': "name",
        'shortName': "shortName",
      });
    });
    //test fromJson
    test("Test fromJson", () {
      final provincia = Provinces.fromJson({
        'url': "url",
        'name': "name",
        'shortName': "shortName",
        'color': "#ffffff",
      }, true);
      expect(provincia.isSelect, true);
      expect(provincia.url, "url");
      expect(provincia.name, "name");
      expect(provincia.shortName, "shortName");
    });
    //test toString
    test("Test toString", () {
      final provincia = Provinces(
        isSelect: true,
        url: "url",
        name: "name",
        shortName: "shortName",
      );
      expect(provincia.toString(),
          "Provinces{url: url, name: name, shortName: shortName}");
    });
    //test operator ==
    test("Test operator ==", () {
      final provincia = Provinces(
        isSelect: true,
        url: "url",
        name: "name",
        shortName: "shortName",
      );
      final provincia2 = Provinces(
        isSelect: true,
        url: "url",
        name: "name",
        shortName: "shortName",
      );
      expect(provincia == provincia2, true);
    });
    //test hashCode
    test("Test hashCode", () {
      final provincia = Provinces(
        isSelect: true,
        url: "url",
        name: "name",
        shortName: "shortName",
      );

      expect(provincia.hashCode, 460425689);
    });
  });
}
