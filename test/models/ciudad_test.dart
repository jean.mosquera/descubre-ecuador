import 'package:descubreecuador/models/city.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group("Test City Model", () {
    test("Test Constructor", () {
      final city = City(name: "name", province: "province");
      expect(city.name, "name");
      expect(city.province, "province");
    });

    test("Test toJson", () {
      final city = City(name: "name", province: "province");
      expect(city.toJson(), {"name": "name", "province": "province"});
    });

    test("Test fromJson", () {
      final city = City.fromJson({"name": "name", "province": "province"});
      expect(city.name, "name");
      expect(city.province, "province");
    });

    test("Test toString", () {
      final city = City(name: "name", province: "province");
      expect(city.toString(), "City{name: name, province: province}");
    });
  });
}
