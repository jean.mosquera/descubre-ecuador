import 'package:descubreecuador/tools/colors.dart';
import 'package:descubreecuador/widget/item_category.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:google_fonts/google_fonts.dart';

main() {
  setUp(() => GoogleFonts.config.allowRuntimeFetching = false);
  testWidgets('SettlementItem', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
          body: ItemCategory(
              color: colors.black, icon: Icons.ac_unit, category: "Categoria")),
    ));

    // Verify that our counter starts at 0.
    expect(find.text('Categoria'), findsOneWidget);
  });
}
