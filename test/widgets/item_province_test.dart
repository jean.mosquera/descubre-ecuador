import 'package:descubreecuador/widget/item_province.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:google_fonts/google_fonts.dart';

main() {
  setUp(() => GoogleFonts.config.allowRuntimeFetching = false);
  testWidgets('SettlementItem', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(const MaterialApp(
      home: Scaffold(
        body: ItemProvince(
          image: '',
          name: 'Provincia de Pichincha',
          isSelect: true,
        ),
      ),
    ));

    // Verify that our counter starts at 0.
    expect(find.text('Provincia de Pichincha'), findsOneWidget);
  });
}
