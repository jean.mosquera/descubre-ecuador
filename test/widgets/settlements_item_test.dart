import 'package:descubreecuador/widget/settlements_item.dart';
import 'package:flutter/material.dart';
import 'package:descubreecuador/models/settlement.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:google_fonts/google_fonts.dart';

main() {
  setUp(() => GoogleFonts.config.allowRuntimeFetching = false);
  /** */
  testWidgets('SettlementItem', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: SettlementItem(
          settlement: Settlement(
              nombreComercial: "Nombre Comercial",
              categoria: "Categoria",
              clasificacion: "Clasificacion",
              direccion: "Direccion",
              canton: "Canton",
              provincia: "Provincia"),
        ),
      ),
    ));

    // Verify that our counter starts at 0.
    expect(find.text('Nombre Comercial'), findsOneWidget);
  });
}
