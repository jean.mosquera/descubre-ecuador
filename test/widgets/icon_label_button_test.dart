import 'package:descubreecuador/widget/icon_label_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:google_fonts/google_fonts.dart';

main() {
  setUp(() => GoogleFonts.config.allowRuntimeFetching = false);
  testWidgets('SettlementItem', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
        home: Scaffold(
            body: IconLabelButton(
                onTap: () {},
                label: "Boton1",
                icon: Icons.ac_unit,
                color: Colors.black,
                isAvailable: true))));

    // Verify that our counter starts at 0.
    expect(find.text('Boton1'), findsOneWidget);
  });
}
